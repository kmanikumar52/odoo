# -*- coding: utf-8 -*-
import json
import logging

import requests
from odoo import models, api
from odoo.osv import osv

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.multi
    def notify_this(self):
        _logger.info("App Update notification send to all users")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        users = self.env['res.users'].search([])
        _logger.info("User count = %s" % len(users))
        for user in users:
            if user.user_tokens:
                for token in self.env['user.token'].browse(user.user_tokens.ids):
                    tokens.append(token.name)
        data_json = {"channeltype": "newAppReleased",
                     "message": "General message: Update Available!"}
        data = {'notification': {'title': "General message: Update Available!",
                                 'body': "Update of app which is Available in Play Store/App store",
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            raise osv.except_osv(('Notification not sent'))
        return True
