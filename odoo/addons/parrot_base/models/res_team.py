
from odoo import fields, models,api,_


class ResTeam(models.Model):
    _name = 'res.team'
    _description = 'Team'

    name = fields.Char('Team Name')
    manager_id = fields.Many2one('res.users', 'Manager')
    members_ids = fields.Many2many('res.users', 'res_team_user_rel', 'uid', 'tid', 'Members')

