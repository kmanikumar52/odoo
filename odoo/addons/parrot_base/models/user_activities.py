# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

import odoo
from odoo import fields, models, api,_
from odoo import SUPERUSER_ID
from odoo import tools
from odoo.modules.module import get_module_resource
from odoo.osv import osv
from odoo.tools.translate import _
import datetime
import time
import json
import requests

_logger = logging.getLogger(__name__)


class UserActivities(models.Model):
    _name = 'user.activities'
    _description = "User Activities"
    _order = "date desc"

    
    name = fields.Many2one('res.users', string='User', required=True)
    category = fields.Many2one(compute='_get_category', string='Category', relation='app.menu')
    current_activity = fields.Char(compute='_get_current_activity', string='Current Activity')
    next_activity = fields.Char(compute='_get_next_activity', string='Previous Activity')
    time_spent = fields.Char('Time Spent')
    from_url = fields.Char("From URL")
    to_url = fields.Char('To URL')
    date = fields.Datetime('Date', default=fields.datetime.now())
    user_log_ids = fields.One2many('user.log', 'user_activities_ids', 'User Log')
    
    # _defaults = {
    #     'date': lambda obj, cr, uid, context: fields.datetime.now(),
    # }

    def _get_time_spent(self):
        res = {}
        for record in self.env['user.activities'].search([('id', 'in', self.ids)]):
            res[record.id] = -1
        return res

    def _get_map(self, url):
        map = {}
        map['/'] = 'Dashboard'
        map['/login'] = 'Login'
        map['/notificationBell'] = 'Notification Bell'
        map['/feedback'] = 'Feedback form'
        map['/dashboard'] = 'Dashboard'
        map['/profile'] = 'Profile Details'
        map['/chat'] = 'User list in Communicate'
        map['/chat/:Id'] = 'Text Chat'
        map['/multiplechat/:Id'] = 'Group Text Chat'
        map['/groupChat/:roomName/:roomId/:type'] = 'Group Call'
        map['/videos/:id'] = 'On Demand Video'
        map['/broadcasts/filter/:filter'] = 'Broadcast'
        map['/broadcasts/:id'] = 'Broadcast'
        map['/broadcasts/:id/list'] = 'Broadcast List'
        map['/videoCall'] = 'Video call'
        map['/performance/:name/:id'] = 'Performance Details'
        map['/performance/:name'] = 'Performance'
        map['/track'] = 'Track Dashboard'
        map['/collaborate'] = 'Project and Tasks Dashboard'
        map['/projects'] = 'List of Projects'
        map['/projects/:projectId'] = 'List of tasks in the specified project'
        map['/tasks'] = 'List of Tasks'
        map['/tasks/:taskId'] = 'Detail view of the specified task'
        map['/createTask'] = 'New Task Creation Form'
        map['/createTask/:editTaskId'] = 'Edit specified Task '
        map['/knowledge'] = 'Knowledge Dashboard'
        map['/knowledge/:menuName'] = 'List of all records in the specified menu'
        map['/knowledgeDetail/:menuName/:recordId'] = 'Details of the specified record in knowledge'
        map['/learn'] = 'Learn Dashboard'
        map['/certify'] = 'Certify Dashboard'
        map['/certify/:certifyMenu'] = 'List of tests or surveys based on the specified menu'
        map['/certifyDetail/:certifyMenu/:certifyId'] = 'Details of test/survey to attend based on specified record id'
        map['/surveys'] = 'List of surveys'
        map['/surveys/:surveyId'] = 'Details of the specified survey'
        map['/attendsurvey/:takeSurveyId'] = 'List of questions in specified survey'
        map['/attendtest/:testId'] = 'List of questions in specified test'
        map['/sales'] = 'Sales Dashboard'
        map['/leads'] = 'List of leads'
        map['/opportunities'] = 'List of opportunities'
        map['/opportunities/:opportunityId'] = 'Information about an opportunity'
        map['/leads/:id'] = 'Details of the specified lead'
        map['/addlead'] = 'New lead Creation'
        map['/addlead/:leadId'] = 'Open lead'
        map['/quotations'] = 'List of quotations'
        map['/quotations/:quotId'] = 'Details of specified quotation'
        map['/addQuotation'] = 'New Quotation Creation'
        map['/addQuotation/:quotationId'] = 'Quotation information'
        map['/hr'] = 'HR'
        map['/retail'] = 'Retail Module'
        map['/retailListNormal'] = 'List of POS'
        map['/retailDetails/:Id'] = 'Details of a POS'
        map['/retailList'] = 'Retail Geo Track - List of POS'
        map['/retailUser/:empName/:empId/:userId'] = 'Retail Geo Track - List of Visit'
        map['/retailVisitList'] = 'List of Visits'
        map['/retailVisitList/:custId'] = 'Visit at a customer'
        map['/retailVisitDetail/:Id'] = 'Visit Information'
        map['/attendvisit/:customerId'] = 'Customer info'
        map['/retailAttachments/:visitId'] = 'Visit attachments'
        map['/addpos'] = 'Pos addition'
        map['/audioBridge/:audioName/:audioId/:roomName'] = 'Audio Bridge'
        map['/geo'] = 'Retail Geo Map - Manager View'
        map['/geoMap'] = 'Retail Geo Map - Employee View'
        map['/hansSurvey/:surveyAnswerId/:patientName'] = 'Respondant Information - Edit/View'
        map['/hansSurvey/:surveyAnswerId'] = 'Respondant Information - New'
        map['/addHospital/:surveyId'] = 'Hospital Information'
        map['/survey-dashboard'] = 'Survey Dashboard'
        map['/patientDetails'] = 'Patient Details'
        map['/certify/:certifyMenu/'] = 'Certification Menu'
        map['/retailVisitList/:assigned_to/:posId'] = 'Retail Visit Assigned To'
        map['/leaveandtravelform'] = 'Leave/Travel Form'
        map['/editVisitAnswer/:visitId'] = 'Leave/Travel Form'
        map['/requestHistory'] = 'Leave/Travel Request History'
        map['/attendvisitlafarge/:backid/:customerId'] = 'POS Visit List'
        map['/addTask/:id'] = 'Add Task'
        map['/attendvisitlafarge/:id/:backid/:customerId'] = 'Attend Visit'
        map['/collabrateProjectTask/:id'] = 'View Project'
        map['/createnewproject'] = 'Create New Project'
        map['/createnewproject/'] = 'Create New Project'
        map['/createTeam'] = 'Create Team'
        map['/createTeam/'] = 'Create Team'
        map['/createteam/:id'] = 'Create Team View'
        map['/geo/:userId'] = 'Geo User'
        map['/knowledge/:menuName/'] = 'Knowledge Menu'
        map['/live_track_view/:name/:latitude/:longitude'] = 'Live Track View'
        map['/projectList/:id'] = 'Project List'
        map['/taskArchival/:id'] = 'Task Archival'
        map['/retailListNormal/:empUserId'] = 'POS List'
        map['/retailListNormal/:id/:nav/:backid'] = 'POS List'
        map['/userDetails'] = 'POS Employee List'
        map['/usersdetails/:id'] = 'POS Employee List'
        map[False] = map['/']
        map[''] = map['/']
        if url in map:
            return map[url]
        else:
            _logger.info('url : ' + str(url) + ' not found')
            return '/'

    def _get_current_activity(self):
        res = {}
        for record in self.env['user.activities'].search([('id', 'in', self.ids)]):
            res[record.id] = self._get_map(record.from_url)
        return res

    def _get_next_activity(self):
        res = {}
        for record in self.env['user.activities'].search([('id', 'in', self.ids)]):
            res[record.id] = self._get_map(record.to_url)
        return res

    def _get_category(self):
        communicate = ['/chat', '/chat/:Id', '/multiplechat/:Id', '/groupChat/:roomName/:roomId/:type', '/videos/:id',
                       '/broadcasts/filter/:filter', '/broadcasts/:id', '/broadcasts/:id/list', '/videoCall']
        track = ['/performance/:name/:id', '/performance/:name', '/track']
        collaborate = ['/collaborate', '/projects', '/projects/:projectId', '/tasks', '/tasks/:taskId', '/createTask',
                       '/createTask/:editTaskId', '/leaveandtravelform', '/createnewproject/', '/projectList/:id',
                       '/collabrateProjectTask/:id', '/taskArchival/:id', '/createteam/:id']
        knowledge = ['/knowledge', '/knowledge/:menuName', '/knowledgeDetail/:menuName/:recordId']
        hr = ['/hr']
        learn = ['/learn']
        certify = ['/certify', '/certify/:certifyMenu', '/certifyDetail/:certifyMenu/:certifyId', '/surveys',
                   '/surveys/:surveyId', '/attendsurvey/:takeSurveyId', '/attendtest/:testId']
        sales = ['/sales', '/leads', '/opportunities', '/opportunities/:opportunityId', '/leads/:id', '/addlead',
                 '/addlead/:leadId', '/quotations', '/quotations/:quotId', '/addQuotation',
                 '/addQuotation/:quotationId']
        retail = ['/retail', '/retailListNormal', '/retailDetails/:Id', '/retailVisitList', '/retailVisitList/:custId',
                  '/retailVisitDetail/:Id', '/attendvisit/:customerId', '/attendvisit/:customerId', '/addpos',
                  '/retailAttachments/:visitId', '/retailListNormal/:empUserId', '/userDetails',
                  '/retailListNormal/:id/:nav/:backid', '/usersdetails/:id', '/attendvisitlafarge/:backid/:customerId',
                  '/live_track_view/:name/:latitude/:longitude', '/retailVisitList/:assigned_to/:posId', ]
        feedback = ['/feedback']
        dashboard = ['/dashboard', '/profile', '/login', '/notificationBell', '/', '']
        retail_geo_track = ['/retailList', '/retailUser/:empName/:empId/:userId', '/geo', '/geoMap']
        surveys = ['/hansSurvey/:surveyAnswerId/:patientName', '/hansSurvey/:surveyAnswerId', '/addHospital/:surveyId',
                   '/survey-dashboard', '/patientDetails']
        res = {}
        for record in self.env['user.activities'].search([('id', 'in', self.ids)]):
            category = ""
            if record.to_url in communicate:
                category = "Communicate"
            elif record.to_url in track:
                category = "Track"
            elif record.to_url in collaborate:
                category = "Collaborate"
            elif record.to_url in knowledge:
                category = "Knowledge"
            elif record.to_url in learn:
                category = "Learn"
            elif record.to_url in certify:
                category = "Certification"
            elif record.to_url in sales:
                category = "Sales"
            elif record.to_url in retail:
                category = "Retail"
            elif record.to_url in feedback:
                category = "Feedback"
            elif record.to_url in dashboard:
                category = "Dashboard"
            elif record.to_url in retail_geo_track:
                category = "Geo Track"
            elif record.to_url in surveys:
                category = "Surveys"
            elif record.to_url in hr:
                category = "HR"
            ids = self.env['app.menu'].search([('name', '=', category)])
            if ids:
                res[record.id] = ids[0]
            else:
                res[record.id] = False
        return res

    
class UserLog(models.Model):
    _name = 'user.log'
    _description = 'User Log'

    
    name = fields.Char('Name')
    log = fields.Text('Log')
    user_activities_ids = fields.Many2one('user.activities', 'User Activities')
    
