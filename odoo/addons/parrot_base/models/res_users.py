import json
import logging
import requests
import time
from datetime import datetime

from odoo import api, models, fields
from odoo.addons.bus.models.bus_presence import AWAY_TIMER
from odoo.addons.bus.models.bus_presence import DISCONNECTION_TIMER
from odoo.osv import osv
from odoo.tools.translate import _

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'
    _inherits = {'res.partner': 'partner_id'}

    phone = fields.Char(string='Phone')
    street = fields.Char(string='Street')
    street2 = fields.Char(string='Street2')
    city = fields.Char(string='city')
    zip = fields.Char(string='zip')
    manager_id = fields.Many2one('res.users', string='Manager')
    team_id = fields.Many2one('res.team', 'Team')
    user_kpi_ids = fields.One2many('res.user.kpi', 'user_id', 'User KPI')
    menu_ids = fields.Many2many('app.menu', 'res_user_menu_rel', 'user_id', 'menu_id', 'Menus')
    user_tokens = fields.Many2many('user.token', 'res_user_token_rel', 'user_id', 'token_id', 'Tokens')
    user_favorites = fields.One2many('user.favorites', 'user_id', 'Favorites')
    emp_id = fields.Char('Employee ID')
    email = fields.Char(related='partner_id.email', inherited=True)
    log_activity = fields.Boolean('Log User Activity',
                                   help='If true All the user activities from mobile application will be captured')
    employee_list = fields.One2many('res.users', 'manager_id', string='Employee List')

    state_id = fields.Many2one('res.country.state', 'State')
    country_id = fields.Many2one('res.country', 'Country')
    mobile_status = fields.Char(string='Mobile Status')
    user_category = fields.Many2one('user.category', string='User Category')

    @api.multi
    def get_db_report_list(self):
        cr = self._cr
        db_name = cr.dbname
        report_list = {}
        general_report = ['User Report', 'Retail Report', 'User Activity Report', 'Track Report', 'Knowledge Report',
                          'Attendance Report', 'POS Visit Report', 'New POS Report']
        if db_name.find("eco") >= 0:
            report_list['report'] = general_report
        else:
            report_list['report'] = general_report + ['Travel Report', 'Order Report', 'Regional Wise Track Report',
                                                      'Conveyance Report',
                                                      'Beat Report']

        return report_list

    @api.multi
    def get_region_selection(self):
        db_name = self._cr.dbname
        if db_name.find("eco") >= 0:
            selection_list = [('manicaland', 'Manicaland'),
                              ('bulawayo', 'Bulawayo'),
                              ('harare_north', 'HARARE NORTH'),
                              ('harare_south', 'HARARE SOUTH'),
                              ('hq', 'HQ'),
                              ('mashonaland_east', 'Mashonaland East & Chitungwiza'),
                              ('masvingo', 'Masvingo'),
                              ('matabeleland', 'Matabeleland'),
                              ('midlands', 'Midlands'),
                              ('mwc', 'MWC'),
                              ('marketing', 'Marketing'),
                              ('parrot', 'Parrot')]
        elif db_name.find("shil") >= 0:
            selection_list = [('east', 'East'),
                              ('north1', 'North 1'),
                              ('north2', 'North 2'),
                              ('south', 'South'),
                              ('west', 'West'),
                              ('e_comm', 'E-comm'),
                              ('modern_trade', 'Modern Trade'),
                              ('b2b', 'B2B'), ('parrot', 'Parrot')
                              ]
        elif db_name.find("rad") >= 0:
            selection_list = [('delhi', 'Delhi'),
                              ('utter_pradesh', 'Utter Pradesh'),
                              ('chennai', 'Chennai'), ('parrot', 'Parrot')
                              ]
        else:
            selection_list = [('demo_region1', 'Demo Region1'),
                              ('demo_region2', 'Demo Region2'), ('parrot', 'Parrot')]
        return selection_list

    @api.model
    def search_communicate_users(self, user_id, limit, offset):
        cr = self.env.cr
        offset = 0
        result_dict = {}
        final_list = []
        result_manager_list = []
        if user_id:
            search_user = self.env['res.users'].search(([('id', '=', user_id)]))
            query = '''select distinct U.id as user_id,
                                    P.id as id,
                                    P.name as name,
                                    CASE WHEN B.last_poll IS NULL THEN 'offline'
                                        WHEN age(now() AT TIME ZONE 'UTC', B.last_poll) > interval %s THEN 'offline'
                                        WHEN age(now() AT TIME ZONE 'UTC', B.last_presence) > interval %s THEN 'away'
                                        ELSE 'online'
                                    END as im_status,  U.region_id as region,
                                    U.user_category as user_cat,
                                    cat.name as cat_name
                                from app_menu app
                                left outer join res_user_menu_rel rel on rel.menu_id=app.id
                                left outer join res_users U on u.id=rel.user_id
                                left outer join user_category cat on cat.id=U.user_category
                                JOIN res_partner P ON P.id = U.partner_id
                                LEFT JOIN bus_presence B ON B.user_id = U.id
                                where app.parent_menu in
                                (select menu.id from app_menu menu where menu.name='Communicate')
                                AND U.id != %s
                                AND U.active = 't'
                               '''
            if search_user.user_category:
                user_category=search_user.user_category.name
                query += ''' AND cat.name=%s ORDER BY U.id'''
                cr.execute(query, ("%s seconds" % DISCONNECTION_TIMER, "%s seconds" % AWAY_TIMER, user_id,user_category))
            else:
                user_category=""
                query += ''' ORDER BY U.id'''
                cr.execute(query, ("%s seconds" % DISCONNECTION_TIMER, "%s seconds" % AWAY_TIMER, user_id))
            result_list = self.env.cr.dictfetchall()
            for data in result_list:
                final_dict = {'user_id': data['user_id'], 'id': data['id'], 'name': data['name'],
                              'im_status': data['im_status'], 'audio': 'T', 'video': 'T', 'chat': 'T'}
                final_list.append(final_dict)
        result_dict.update({'result': final_list})
        return result_dict

    @api.model
    def online_offline_status(self, user_id):
        _logger.info("Live location was triggered")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        usr_sr = self.search([('id', '=', user_id)])
        if usr_sr:
            send = True
            tokens = []
            if usr_sr.user_tokens:
                for token in usr_sr.user_tokens:
                    tokens.append(token.name)
            else:
                usr_sr.mobile_status = 'Offline'
            if len(tokens) > 0:
                data_json = {"channeltype": "OnlineOfflineStatus",
                             "name": usr_sr.name,
                             "author_id": usr_sr.id,
                             "message": "Please open the App"}
                data = {'data': {'title': "Please open the App",
                                 'body': "Please open the App",
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                        'data': data_json,
                        'registration_ids': tokens,
                        'priority': 'high',
                        'forceShow': 'true'}
                headers = {'Authorization': fcm_authorization_key,
                           'Content-Type': 'application/json'}
                _logger.info("Input Data ------ %s" % data)
                data = json.dumps(data)
                try:
                    if send:
                        _logger.info("FCM Response ------------------------------------------------------------")
                        # res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                        # _logger.info("FCM Response ------ %s" % res.text)
                        # time.sleep(3)
                        # self.mobile_status_update(user_id, 'offline')
                except:
                    raise osv.except_osv(_('Notification not sent'))
                    self.mobile_status_update(user_id, 'offline')

    @api.model
    def mobile_status_update(self, user_id, status):
        usr_sr = self.search([('id', '=', user_id)])
        if status == 'online':
            # usr_sr.write({'mobile_status': 'Online'})
            res = {'result': 'Online'}
        elif status == 'background':
            today = (datetime.today()).strftime('%Y-%m-%d %H:%M')
            last_seen = 'Last seen ' + today
            # usr_sr.write({'mobile_status': last_seen})
            res = {'result': last_seen}
        elif status == 'offline':
            # usr_sr.write({'mobile_status': 'Offline'})
            res = {'result': 'Offline'}
        else:
            # usr_sr.write({'mobile_status': 'Offline'})
            res = {'result': 'Offline'}
        return res

    @api.model
    def get_mobile_status_update(self, user_id):
        res = {}
        if user_id:
            usr_sr = self.search([('partner_id', '=', int(user_id))])
            res = {'result': ''}
        return res


class UserCategory(models.Model):
    _name = 'user.category'

    name = fields.Char(string='Name')
    active = fields.Boolean(string='Active')

class UserRegion(models.Model):
    _name = 'user.region'

    name = fields.Char(string='Name')
    value = fields.Char(string='Value')
    active = fields.Boolean(string='Active')


class UserFavorites(models.Model):
    _name = 'user.favorites'
    _description = 'Logged User favorite Users'

    name = fields.Many2one('res.users', 'Favorite User')
    user_id = fields.Many2one('res.users', 'User')
    partner_id = fields.Many2one("res.partner", 'Partner')


#     def delete_favorites(self, cr, uid, ids, fav_user_id, context=None):
#         if fav_user_id:
#             favorite_ids = self.search(cr, uid, [('name', '=', fav_user_id), ('user_id', '=', uid)])
#             if favorite_ids:
#                 self.unlink(cr, uid, favorite_ids)
#         return True

class UserToken(models.Model):
    _name = 'user.token'
    _description = 'Logged User tokens form different devices'


    name = fields.Char('Token')
    device_id = fields.Char('Device Id')
    device_type = fields.Char('Device Type')
    app_version = fields.Char('App Version')


    def create(self,vals):
        if 'device_id' in vals:
            if 'device_type' in vals and vals['device_type']== 'Android':
                user_sr = self.env['res.users'].search([('id', '=', self.id)])
                user_br = self.env['res.users'].browse(user_sr)
                if len(user_br.user_tokens) > 0:
                    fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
                    send = True
                    tokens = []
                    for token in user_br.user_tokens:
                        tokens.append(token.name)
                    msg = "Sorry!! you have been logged out due to the use of your credentials by another person."
                    data_json = {"channeltype": "LogOut",
                                 "message": msg}
                    data = {'notification': {'title': "Log out",
                                             'body': msg,
                                             'sound': 'default',
                                             'click_action': 'FCM_PLUGIN_ACTIVITY',
                                             'icon': 'fcm_push_icon',
                                             'color': '#F8880D'},
                            'data': data_json,
                            'registration_ids': tokens,
                            'priority': 'high',
                            'forceShow': 'true',
                            'restricted_package_name': ''}
                    headers = {'Authorization': fcm_authorization_key,
                               'Content-Type': 'application/json'}
                    _logger.info("Input Data ------ %s" % data)
                    data = json.dumps(data)
                    try:
                        if send:
                            res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                            _logger.info("FCM Response ------ %s" % res.text)
                    except:
                        raise osv.except_osv(_('Notification not sent'))
                    for token in user_br.user_tokens:
                        self.unlink(token.id)
            else:
                user_sr = self.env['res.users'].search([('id', '=', self.id)])
                user_br = self.env['res.users'].browse(user_sr)
                if len(user_br.user_tokens) > 1:
                    fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
                    send = True
                    tokens = []
                    for token in user_br.user_tokens:
                        tokens.append(token.name)
                    msg = "Sorry!! you have been logged out due to the use of your credentials by another person."
                    data_json = {"channeltype": "LogOut",
                                 "message": msg}
                    data = {'notification': {'title': "Log out",
                                             'body': msg,
                                             'sound': 'default',
                                             'click_action': 'FCM_PLUGIN_ACTIVITY',
                                             'icon': 'fcm_push_icon',
                                             'color': '#F8880D'},
                            'data': data_json,
                            'registration_ids': tokens,
                            'priority': 'high',
                            'forceShow': 'true',
                            'restricted_package_name': ''}
                    headers = {'Authorization': fcm_authorization_key,
                               'Content-Type': 'application/json'}
                    _logger.info("Input Data ------ %s" % data)
                    data = json.dumps(data)
                    try:
                        if send:
                            res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                            _logger.info("FCM Response ------ %s" % res.text)
                    except:
                        raise osv.except_osv(_('Notification not sent'))
                    for token in user_br.user_tokens:
                        self.unlink(token.id)
        return super(UserToken, self).create(vals)

class AppReleaseVersion(models.Model):
    _name = 'app.release.version'

    name = fields.Char(string='App Release Version')
    date_interval = fields.Integer(string='Date Interval')

    @api.multi
    def get_app_release_version(self):
        result = {}
        app_sr = self.search([], order="id desc", limit=1)
        result.update({'app_release_version': app_sr.name})
        return result


class UserAppVersion(models.Model):
    _name = 'user.app.version'

    user_id = fields.Many2one('res.users', string="User Name")
    app_version = fields.Char(string="Version")

    @api.multi
    def mobile_create(self, vals):
        if 'user_id' in vals and 'app_version' in vals:
            res_obj = self.search([('user_id', '=', vals['user_id'])], order='id desc', limit=1)
            if res_obj:
                res_obj.write(vals)
            else:
                self.create(vals)
        else:
            self.create(vals)
        return True

    @api.multi
    def send_remind_me_notification(self):
        _logger.info("App Update notification send to all users who gave remind me later")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        res = self.env['app.release.version'].get_app_release_version()
        users_app_obj = self.search([('app_version', '!=', res['app_release_version'])])
        for app in users_app_obj:
            users = self.env['res.users'].search([('id', '=', app.user_id.id)])
            for user in users:
                if user.user_tokens:
                    for token in self.env['user.token'].browse(user.user_tokens.ids):
                        if token.device_type !='iOS':
                            tokens.append(token.name)
            data_json = {"channeltype": "newAppReleased",
                         "message": "You have new update in playstore. Do you want to update?"}
            data = {'notification': {'title': "You have new update in playstore. Do you want to update?",
                                     'body': "You have new update in playstore. Do you want to update?",
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))
        return True
