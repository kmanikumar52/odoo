# -*- coding: utf-8 -*-

from . import app_menu
from . import res_users
from . import res_partner
from . import res_team
from . import res_user_kpi
from . import user_activities