import base64
import datetime
import json
import logging
import os
from datetime import timedelta

import pytz
import requests
import xlsxwriter
from odoo import fields,models, api,_
from odoo.osv import osv
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT
from odoo.tools.translate import _

_logger = logging.getLogger(__name__)


class Test(models.Model):
    _name = 'test'
    _description = 'Tests'

    def _get_survey_info(self):
        if not self.ids: return {False}
        res = {}
        survey_obj = self.env['survey.question.bank'].search([('id','in', self.ids)])
        for test in survey_obj:
            if test.survey_question:
                for record in test.survey_question:
                    survey_ids = []
                    survey_ids = survey_obj.search([('id', '=', record.id)])
                    display_text = '''
                                    <html>
                                        <head>
                                            <style>
                                                table.table-condensed {
                                                    counter-reset: rowNumber;
                                                    border-radius: 10px;
                                                    background: #B1B5B8;
                                                    box-shadow: 0px 0px 10px #0a0909;
                                                }
                                                table.table-condensed tr:not(:first-child) {
                                                    counter-increment: rowNumber;
                                                }
                                                table.table-condensed tr td:first-child::before {
                                                    content: counter(rowNumber);
                                                    min-width: 1em;
                                                    margin-right: 1.5em;
                                                }
                                            </style>
                                        </head>
                                        <body>
                                            <table class="table table-condensed">
                                                <tr>
                                                    <th>No. | Questions</th>
                                                    <th>Sequence</th>
                                                    <th>Type</th>
                                                    <th>For Answer</th>
                                                    <th>Parent Question</th>
                                                </tr>'''
                    for survey in test.survey_question:
                        display_text += '<tr><td>' + survey.name + '</td>'
                        display_text += '<td>' + str(survey.sequence) + '</td>'
                        display_text += '<td>' + str(survey.type) + '</td>'
                        display_text += '<td> - - - - - </td>'
                        display_text += '<td> - - - - - </td></tr>'
                        # Description
                        while survey.option_desc.question_id.id:
                            display_text += '<tr><td>' + survey.option_desc.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_desc.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_desc.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_desc.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_desc.question_id
                        # Yes/No and Multiple
                        while survey.option_a.question_id:
                            display_text += '<tr><td>' + survey.option_a.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_a.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_a.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_a.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_a.question_id
                        while survey.option_b.question_id:
                            display_text += '<tr><td>' + survey.option_b.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_b.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_b.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_b.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_b.question_id
                        while survey.option_c.question_id:
                            display_text += '<tr><td>' + survey.option_c.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_c.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_c.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_c.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_c.question_id
                        while survey.option_d.question_id:
                            display_text += '<tr><td>' + survey.option_d.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_d.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_d.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_d.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_d.question_id
                        while survey.option_e.question_id:
                            display_text += '<tr><td>' + survey.option_e.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_e.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_e.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_e.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_e.question_id
                        while survey.option_f.question_id:
                            display_text += '<tr><td>' + survey.option_f.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_f.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_f.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_f.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_f.question_id
                        while survey.option_g.question_id:
                            display_text += '<tr><td>' + survey.option_g.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_g.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_g.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_g.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_g.question_id
                        while survey.option_h.question_id:
                            display_text += '<tr><td>' + survey.option_h.question_id.name + '</td>'
                            display_text += '<td>' + str(survey.option_h.question_id.sequence) + '</td>'
                            display_text += '<td>' + str(survey.option_h.question_id.type) + '</td>'
                            display_text += '<td>' + survey.option_h.name + '</td>'
                            display_text += '<td>' + survey.name + '</td></tr>'
                            survey = survey.option_h.question_id
                    display_text += '''     </table>
                                        </body>
                                    </html>'''
                    res[test.id] = display_text
            elif test.questions:
                display_text = ''' '''
                res[test.id] = display_text
        return res


    name = fields.Char('Name', required=True)
    type = fields.Selection([('time_bound', 'Time Bound Test'),
                              ('one_by_one', 'One By One Test'),
                              ('time_bound_question', 'Time Bound Questions'),
                              ('adaptive', 'Adaptive Test')], 'Type')
    test_time = fields.Float('Time (in min)',default=1)
    description = fields.Text('Description', required=True)
    menutype = fields.Selection([('learning', 'Learning Modules with Tests'),
                                  ('survey', 'Survey'),
                                  ('poll', 'Poll Participation'),
                                  ('answer', 'Answer a Question')], 'Menu Type')
    questions = fields.Many2many('product.question.bank', 'test_questions_rel', 'question_id', 'test_id',
                                  'Questions')
    pass_percentage = fields.Integer('Pass Percentage',
                                      help="This indicates the percentage above which test is considered as Pass.", default=40)
    survey_question = fields.Many2many('survey.question.bank', 'survey_questions_rel', 'questions_id', 'survey_id',
                                        'Questions')
    child_id = fields.Many2one('test', 'Sub Test')
    survey_frequency = fields.Integer('Frequency(days)', default=1)
    survey_last_updated = fields.Datetime("Last Updated", default=fields.datetime.now())
    survey_start = fields.Datetime("Start Date", default=fields.datetime.now())
    survey_end = fields.Datetime("End Date", default=fields.datetime.now() + timedelta(days=7))
    survey_time = fields.Float("Survey Time")
    survey_results_download_xl = fields.Many2many('ir.attachment', 'survey_results_xl_down',
                                                   'survey_results_xl_down_id', 'attachment_id',
                                                   'Download Results', readonly=True)
    user_surveys = fields.Many2many('user.surveys', 'test_user_surveys', 'test_id', 'survey_id', 'Users')
    survey_info = fields.Html(compute='_get_survey_info', string='Survey Info')
    report_start_date = fields.Date('Report Start Date', default=fields.date.today() - timedelta(days=7))
    report_end_date = fields.Date('Report End Date', default=fields.date.today())
    user_id = fields.Many2many('res.users', 'test_user_rel', 'test_id', 'user_id', string='Users')


#     _defaults = {
#         'pass_percentage': 40,
#         'test_time': 1,
#         'survey_last_updated': lambda obj, cr, uid, context: fields.datetime.now(),
#         'report_start_date': datetime.datetime.today() - timedelta(days=7),
#         'report_end_date': datetime.datetime.today(),
#         'survey_start': datetime.datetime.now(),
#         'survey_end': datetime.datetime.now() + timedelta(days=7),
#         'survey_frequency': 1,
#     }

    def download_survey_results(self):
        context = self._context or {}
        company_obj = self.env['res.users'].browse(self.uid).company_id
        temp = company_obj.temp_files_path
        if not temp:
            company_write = self.env['res.company'].write(self.uid, [company_obj.id],
                                                               {"temp_files_path": "/home/parrot/"})
            temp = "/home/parrot/"
        if not os.path.exists(temp + 'Samples'):
            os.makedirs(temp + 'Samples')
        temp = temp + 'Samples/'
        sample_xls = xlsxwriter.Workbook(temp + 'sample_document.xls')
        sheet = sample_xls.add_worksheet()
        border_format = sample_xls.add_format({'border': 2,
                                               'bold': True,
                                               'bg_color': '#90af48'})
        sheet.write('A1', 'User', border_format)
        sheet.write('B1', 'Date and Time', border_format)
        survey_ques_sr = self.env['survey.question.bank'].search([('id','=', self.id)])
        survey_ques_br = self.env['survey.question.bank'].browse(survey_ques_sr)
        row_head = 0
        col_head = 2
        test_ids = self.browse(self.uid, self.ids)
        test_result = self.env['test.results']
        res_ids = test_result.search(self.uid, [('survey_test', 'in', test_ids.ids),
                                               ('create_date', '>=', test_ids.report_start_date),
                                               ('create_date', '<=', test_ids.report_end_date)])
        result_ids = test_result.browse(self.uid, res_ids)
        for survey_ques in survey_ques_br:
            sheet.write(row_head, col_head, survey_ques.name, border_format)
            col_head += 1
        row = 1
        col = 0
        for res in result_ids:
            sheet.write(row, col, res.user_id.name)
            col += 1
            timezone = pytz.timezone(context.get('tz') or 'UTC')
            create_date = pytz.UTC.localize(datetime.datetime.strptime(res.create_date, DATETIME_FORMAT)).astimezone(
                timezone).strftime("%Y-%m-%d %H:%M:%S")
            sheet.write(row, col, create_date)
            col += 1
            for survey_ques in survey_ques_br:
                for quest in res.survey_ques_and_ans_ids:
                    if survey_ques.id == quest.name.id:
                        sheet.write(row, col, quest.choosen_survey_answer)
                    else:
                        sheet.write(row, col, '')
                col += 1
            row += 1
            col = 0
        sample_xls.close()
        with open(temp + 'sample_document.xls', 'rb') as doc:
            encoded_data = base64.b64encode(doc.read())
        document_vals = {'name': 'survey_results_file.xls',
                         'datas': encoded_data,
                         'datas_fname': 'survey_results_file.xls',
                         'res_model': 'test',
                         'res_id': ids[0],
                         'type': 'binary'}
        ir_id = self.env['ir.attachment'].create(document_vals)
        self.write({'survey_results_download_xl': [(6, 0, [ir_id])]})

    def get_subtest(self, child_id, count=0):
        count += 1
        for record in self.browse(child_id):
            if record.child_id:
                count = self.get_subtest(record.child_id.id, count)
            else:
                return count
        return count

    @api.model
    def create(self,vals):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        name = vals['name']
        user_ids = self.env['res.users'].search([])
        result = super(Test, self).create(vals)
        test_id = result
        tokens = []
        for record in self.env['test'].search([('id', 'in', test_id.ids)]):
            menutype = record.menutype
        if 'child_id' in vals:
            for record in self.browse(vals['child_id']):
                count = self.get_subtest(vals['child_id'])
                if count >= 2:
                    raise osv.except_osv(('Warning'), ("The %s already has a subtest!!") % (record.name))
        for user in self.env['res.users'].search([('id','in',user_ids.ids)]):
            if user.user_tokens:
                for token in self.env['user.token'].search([('id','in',user.user_tokens.ids)]):
                    tokens.append(token.name)
        if 'type' in vals and vals['type']:
            data_json = {"channeltype": "newTest",
                         "name": name,
                         "message": "New Test %s has been created" % name,
                         "test_id": test_id.ids,
                         'menutype': menutype}
            data = {'notification': {'title': name,
                                     'body': "New Test %s has been created" % name,
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
        else:
            data_json = {"channeltype": "newTest",
                         "name": name,
                         "message": "New Survey %s has been created" % name,
                         "date": vals['survey_start'],
                         "test_id": test_id.ids,
                         'menutype': menutype}
            data = {'notification': {'title': name,
                                     'body': "New Survey  %s has been created" % name,
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return result

    def write(self, vals):
        if 'child_id' in vals:
            for record in self.browse(vals['child_id']):
                count = self.get_subtest(vals['child_id'])
                if count >= 2:
                    raise osv.except_osv(('Warning'), ("The %s already has a subtest!!") % record.name)
        return super(Test, self).write(vals)

    def unlink(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        for page_id in self.env['test'].search([('id','in', self.ids)]):
            test_id = page_id.id
            for record in self.browse(test_id):
                menutype = record.menutype
            name = page_id.name
            user_ids = self.env['res.users'].search([])
            for user in self.env['res.users'].browse(user_ids):
                tokens = []
                if user.user_tokens:
                    for token in self.env['user.token'].browse(user.user_tokens.ids):
                        tokens.append(token.name)
            if hasattr(page_id, 'type') and page_id.type == True:
                data_json = {"channeltype": "newTest",
                             "name": name,
                             "message": "The Test %s has been removed" % name,
                             'test_id': test_id,
                             'menutype': menutype}
                data = {'notification': {'title': name,
                                         'body': "The Test %s has been removed" % name,
                                         'sound': 'default',
                                         'click_action': 'FCM_PLUGIN_ACTIVITY',
                                         'icon': 'fcm_push_icon'},
                        'data': data_json,
                        'registration_ids': tokens,
                        'priority': 'high',
                        'forceShow': 'true',
                        'restricted_package_name': ''}
            else:
                data_json = {"channeltype": "newTest",
                             "name": name,
                             "message": "The Survey  %s has been removed" % name,
                             'test_id': test_id,
                             'menutype': menutype}
                data = {'notification': {'title': name,
                                         'body': "The Survey  %s has been removed" % name,
                                         'sound': 'default',
                                         'click_action': 'FCM_PLUGIN_ACTIVITY',
                                         'icon': 'fcm_push_icon'},
                        'data': data_json,
                        'registration_ids': tokens,
                        'priority': 'high',
                        'forceShow': 'true',
                        'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))
        return super(Test, self).unlink(self.uid, self.ids)

    def _update_survey_frequency(self):
        survey_ids = self.env['test'].search([('menutype', '=', 'survey')])
        today = datetime.datetime.today()
        for survey in self.read(survey_ids, ['id', 'name', 'type', 'survey_question', 'description',
                                                      'menutype', 'survey_last_updated', 'survey_frequency',
                                                      'survey_start', 'survey_end', 'survey_time']):
            survey_freq = survey['survey_frequency']
            if survey['survey_last_updated']:
                last_updated = datetime.datetime.strptime(survey['survey_last_updated'], DATETIME_FORMAT)
                if survey_freq == 'daily':
                    if last_updated < today:
                        self.write([survey['id']], {'survey_last_updated': today})
                if survey_freq == 'weekly':
                    if last_updated + datetime.timedelta(days=7) < today:
                        self.write([survey['id']], {'survey_last_updated': today})
                if survey_freq == 'monthly':
                    if last_updated + datetime.timedelta(days=30) < today:
                        self.write([survey['id']], {'survey_last_updated': today})
        return True

    def check_registration(self):
        results_pool = self.env['test.results']
        for test in self.env['test'].search([]):
            registered_ids = results_pool.search([('name', '=', test.id), ('user_id', '=', self.id)])
            if len(registered_ids) > 0:
                return True
            else:
                return False

    def register_for_test(self):
        tp_ids = False
        no_of_test_attended = 0.0
        results_pool = self.env['test.results']
        tp_pool = self.env['test.progress']
        test_questions_pool = self.env['test.question.answer']
        test_ques_ids = []
        for test in self.env['test'].search([('id','in',self.ids)]):
            if not self.check_registration([test.id]):
                for ques in test.questions:
                    test_ques_ids.append(test_questions_pool.create({'name': ques.id}))
                tp_id = tp_pool.search([('user_id', '=', self.id)])
                if tp_id:
                    tp_ids = tp_id[0]
                else:
                    tp_ids = tp_pool.create({'user_id': uid, 'name': test.id})
                result_id = results_pool.create({'name': test.id,
                                                          'user_id': uid,
                                                          'testprogress_id': tp_ids,
                                                          'ques_and_ans_ids': [(6, 0, test_ques_ids)],
                                                          'status': 'new'})
                test_p = tp_pool.browse(tp_ids)
                no_of_test_attended = len(test_p.testresult_id)
                total_tests = len(self.env['test'].search([('menutype', '=', 'learning')]))
                percent_completed = ((no_of_test_attended * 100) / total_tests)
                tp_pool.write(tp_ids, {'completion': percent_completed})
                return result_id
            else:
                return False

    # def get_surveys(self):
    #     surveys = []
    #     today = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    #     survey_ids = self.env['test'].search([('menutype', '=', 'survey'), ('survey_end', '>=', today)])
    #     for survey in self.read(survey_ids, ['name', 'type', 'survey_question', 'description', 'user_surveys',
    #                                                   'survey_start', 'menutype', 'survey_last_updated', 'user_id',
    #                                                   'survey_frequency']):
    #         survey['user_survey_details'] = []
    #         user_surveys = self.env['user.surveys'].search([('id', 'in', survey['user_surveys'])])
    #         for survey_user in user_surveys:
    #             survey['user_survey_details'].append(
    #                 {"user_id": survey_user['user_id'][0], "checked": survey_user['checked']})
    #         _logger.info("User Survey details %s", survey['user_survey_details'])
    #         survey['res_users_details'] = []
    #         for user_id in survey['user_id']:
    #             res_users = self.env['res.users'].search([('id', '=', user_id)])
    #             for res_user in res_users:
    #                 survey['res_users_details'].append({"user_id": res_user['id'], "checked": res_user['checked']})
    #         _logger.info("Res user details %s", survey['res_users_details'])
    #         survey_attempts = self.env['test.results'].search([('user_id', '=', uid),
    #                                                                               ('survey_test', '=', survey['id'])],
    #                                                                     order='create_date DESC')
    #         _logger.info(surveys)
    #         if len(survey_attempts) < survey['survey_frequency']:
    #             surveys.append(survey)
    #         elif len(survey_attempts) == 0:
    #             surveys.append(survey)
    #     return {"records": surveys}

    def get_surveys(self):
        today = datetime.datetime.now().strftime("%Y-%m-%d")
        cr.execute('''DROP FUNCTION IF EXISTS get_survey()''')
        cr.execute('''CREATE OR REPLACE FUNCTION get_survey()
                          RETURNS TABLE (
                           tst_id int,
                           ques_id int,
                           tst_name character varying,
                           tst_type character varying,
                           description text,
                           survey_start timestamp,
                           menutype character varying,
                           survey_last_updated timestamp,
                           survey_frequency int,
                           user_id int,
                           login character varying,
                           checked boolean
                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                            select tst.id as tst_id,
                            ques.id as ques_id,
                            tst.name as tst_name,
                            tst.type as tst_type,
                            tst.description as description,
                            tst.survey_start as survey_start,
                            tst.menutype as menutype,
                            tst.survey_last_updated as survey_last_updated,
                            tst.survey_frequency as survey_frequency,
                            usr_rel.user_id as user_id,
                            users.login as login,
                            users.checked as checked
                            from test tst
                            left outer join survey_questions_rel que_rel on que_rel.questions_id=tst.id
                            left outer join survey_question_bank ques on ques.id=que_rel.survey_id
                            left outer join test_user_rel usr_rel on usr_rel.test_id=tst.id
                            left outer join res_users users on users.id=usr_rel.user_id
                            where tst.menutype='survey' and tst.survey_end >=%s ;
                            END
                            $func$ LANGUAGE plpgsql; ''', (today,))

        cr.execute('''select distinct tst_id from get_survey() order by tst_id''')
        survey_dict = cr.fetchall()
        survey_lst=[]
        for survey in survey_dict:
            survey_lst.append(survey[0])
        res_list = []
        cr.execute('''select distinct tst_id from get_survey() 
                        Except
                        select survey_test from test_results res 
                        where res.survey_test in %s and res.user_id=%s''',(tuple(survey_lst),uid))
        completed_survey=cr.fetchall()
        for survey in completed_survey:
            cr.execute(
                '''select Distinct survey_frequency,tst_id as id,
                tst_name as name,survey_start,description,
                survey_last_updated from get_survey() 
                where tst_id=%s''',
                (survey[0],))
            survey_res = cr.dictfetchall()
            cr.execute(
                '''select Distinct user_id,checked from get_survey() where tst_id=%s''',
                (survey[0],))
            survey_users= cr.fetchall()
            user_list=[]
            user_survey_detail_list=[]
            for user in survey_users:
                user_list.append(user[0])
                user_survey_detail_list.append({'checked': user[1], 'user_id': user[0]})
            user_survey_list=[]
            survey_question=[]
            cr.execute(
                '''select Distinct ques_id from get_survey() where tst_id=%s''',
                (survey[0],))
            survey_question_all= cr.fetchall()
            for ques in survey_question_all:
                survey_question.append(ques[0])
            for res in survey_res:
                res_dict = {}
                res_dict.update(res)
                res_dict.update({'user_id':user_list})
                res_dict.update({'user_survey_details':user_survey_detail_list})
                res_dict.update({'user_surveys':user_survey_list})
                res_dict.update({'survey_question':survey_question})
                res_list.append(res_dict)
        return {"records": res_list}

    def get_poll(self):
        polls = []
        now = datetime.datetime.now()
        today = now.strftime("%Y-%m-%d %H:%M")
        poll_ids = self.env['test'].search([('menutype', '=', 'poll')])
        # poll_ids = self.search(cr, uid, [('menutype', '=', 'poll'), ('survey_end', '>=', today)])
        for survey in self.read(poll_ids, ['name', 'type', 'survey_question', 'description', 'user_surveys',
                                                    'survey_start', 'menutype', 'survey_last_updated', 'user_id',
                                                    'survey_frequency']):
            survey['user_survey_details'] = []
            user_surveys = self.env['user.surveys'].search([('id', 'in', survey['user_surveys'])])
            for survey_user in user_surveys:
                survey['user_survey_details'].append(
                    {"user_id": survey_user['user_id'][0], "checked": survey_user['checked']})
            _logger.info("User Survey details %s", survey['user_survey_details'])
            survey['res_users_details'] = []
            for user_id in survey['user_id']:
                res_users = self.env['res.users'].search([('id', 'in', user_id)])
                for res_user in res_users:
                    survey['res_users_details'].append({"user_id": res_user['id'], "checked": res_user['checked']})
            _logger.info("Res user details %s", survey['res_users_details'])
            survey_attempts = self.env['test.results'].search([('user_id', '=', self.id),
                                                                                  ('survey_test', '=', survey['id'])],
                                                                        order='create_date DESC')
            if len(survey_attempts) > 0:
                last_attempt_date = survey_attempts[0]['create_date']
                if survey['survey_last_updated'] > last_attempt_date:
                    polls.append(survey)
            elif len(survey_attempts) == 0:
                polls.append(survey)
        return {"records": polls}

    def get_answer(self):
        answers = []
        now = datetime.datetime.now()
        today = now.strftime("%Y-%m-%d %H:%M")
        answer_ids = self.env['test'].search([('menutype', '=', 'answer')])
        # answer_ids = self.search(cr, uid, [('menutype', '=', 'answer'), ('survey_end', '>=', today)])
        for survey in self.read(answer_ids, ['name', 'type', 'survey_question', 'description', 'user_surveys',
                                                      'survey_start', 'menutype', 'survey_last_updated',
                                                      'survey_frequency']):
            survey['user_survey_details'] = []
            user_surveys = self.env['user.surveys'].search([('id', 'in', survey['user_surveys'])])
            for survey_user in user_surveys:
                survey['user_survey_details'].append(
                    {"user_id": survey_user['user_id'][0], "checked": survey_user['checked']})
            _logger.info("User Survey details %s", survey['user_survey_details'])
            survey['res_users_details'] = []
            for user_id in survey['user_id']:
                res_users = self.env['res.users'].search([('id', 'in', user_id)])
                for res_user in res_users:
                    survey['res_users_details'].append({"user_id": res_user['id'], "checked": res_user['checked']})
            _logger.info("Res user details %s", survey['res_users_details'])
            survey_attempts = self.env['test.results'].search([('user_id', '=', self.id),('survey_test', '=', survey['id'])],
                                                                        order='create_date DESC')
            if len(survey_attempts) > 0:
                last_attempt_date = survey_attempts[0]['create_date']
                if survey['survey_last_updated'] > last_attempt_date:
                    answers.append(survey)
            elif len(survey_attempts) == 0:
                answers.append(survey)
        return {"records": answers}


class TestQuestionAnswer(models.Model):
    _name = 'test.question.answer'
    _description = 'Test Question Answers'


    name = fields.Many2one('product.question.bank', 'Question', required=True)
    answer = fields.Selection([('option_a', 'Option 1'), ('option_b', 'Option 2'),
                                ('option_c', 'Option 3'), ('option_d', 'Option 4'),
                                ('option_e', 'Option 5'), ('option_f', 'Option 6'),
                                ('option_g', 'Option 7'), ('option_h', 'Option 8'),
                                ('option_i', 'Option 9'), ('option_j', 'Option 10'),
                                ('option_k', 'Option 11'), ('option_l', 'Option 12'),
                                ('option_m', 'Option 13'), ('option_n', 'Option 14'),
                                ('option_o', 'Option 15'), ('option_p', 'Option 16'),
                                ('option_q', 'Option 17'), ('option_r', 'Option 18'),
                                ('option_s', 'Option 19'), ('option_t', 'Option 20')], 'Answer')
    result = fields.Selection([('correct', 'Correct'), ('wrong', 'Wrong')], 'Result')


    def check_answer(self):
        for answer in self.env['test.question.answer'].search([('id','in',self.ids)]):
            if answer.answer == answer.name.correct_answer:
                self.write({'result': 'correct'})
                return True
            else:
                self.write({'result': 'wrong'})
                return False


class TestProgress(models.Model):
    _name = 'test.progress'
    _description = 'Test Progress'
    _order = 'id desc'


    name = fields.Many2one('test', 'Test')
    user_id = fields.Many2one('res.users', 'User')
    completion = fields.Float("% Completed")
    testresult_id = fields.One2many('test.results', 'testprogress_id', 'Test Result')
    test_or_progress = fields.Selection([('test', 'Test'), ('progress', 'Progress')], 'Test/Progress', default='progress')


class SurveyTestQuestionAnswer(models.Model):
    _name = 'survey.test.question.answer'
    _description = 'Survey Test Question Answers'
    _order = 'sequence'

    def _get_survey_answer(self, name, args):
        if not ids:
            return {False}
        res = {}
        for record in self.env['survey.test.question.answer'].search([('id','in',self.ids)]):
            if record.name.type == 'multiple':
                if record.answer == 'option_a':
                    res[record.id] = record.name.option_a[0].name
                elif record.answer == 'option_b':
                    res[record.id] = record.name.option_b[0].name
                elif record.answer == 'option_c':
                    res[record.id] = record.name.option_c[0].name
                elif record.answer == 'option_d':
                    res[record.id] = record.name.option_d[0].name
                elif record.answer == 'option_e':
                    res[record.id] = record.name.option_e[0].name
                elif record.answer == 'option_f':
                    res[record.id] = record.name.option_f[0].name
                elif record.answer == 'option_g':
                    res[record.id] = record.name.option_g[0].name
                elif record.answer == 'option_h':
                    res[record.id] = record.name.option_h[0].name
                else:
                    res[record.id] = False
            elif record.name.type == 'selection':
                if record.answer == 'option_a':
                    res[record.id] = record.name.option_a[0].name
                elif record.answer == 'option_b':
                    res[record.id] = record.name.option_b[0].name
                elif record.answer == 'option_c':
                    res[record.id] = record.name.option_c[0].name
                elif record.answer == 'option_d':
                    res[record.id] = record.name.option_d[0].name
                elif record.answer == 'option_e':
                    res[record.id] = record.name.option_e[0].name
                elif record.answer == 'option_f':
                    res[record.id] = record.name.option_f[0].name
                elif record.answer == 'option_g':
                    res[record.id] = record.name.option_g[0].name
                elif record.answer == 'option_h':
                    res[record.id] = record.name.option_h[0].name
                elif record.answer == 'option_i':
                    res[record.id] = record.name.option_i[0].name
                elif record.answer == 'option_j':
                    res[record.id] = record.name.option_j[0].name
                elif record.answer == 'option_k':
                    res[record.id] = record.name.option_k[0].name
                elif record.answer == 'option_l':
                    res[record.id] = record.name.option_l[0].name
                elif record.answer == 'option_m':
                    res[record.id] = record.name.option_m[0].name
                elif record.answer == 'option_n':
                    res[record.id] = record.name.option_n[0].name
                elif record.answer == 'option_o':
                    res[record.id] = record.name.option_o[0].name
                elif record.answer == 'option_p':
                    res[record.id] = record.name.option_p[0].name
                elif record.answer == 'option_q':
                    res[record.id] = record.name.option_q[0].name
                elif record.answer == 'option_r':
                    res[record.id] = record.name.option_r[0].name
                elif record.answer == 'option_s':
                    res[record.id] = record.name.option_s[0].name
                elif record.answer == 'option_t':
                    res[record.id] = record.name.option_t[0].name
                else:
                    res[record.id] = False
            elif record.name.type == 'multi_select':
                res[record.id] = record.multi_select
            elif record.name.type == 'yes_no':
                res[record.id] = record.yes_no
            elif record.name.type == 'descriptive':
                res[record.id] = record.survey_description
            elif record.name.type == 'title':
                res[record.id] = record.survey_description
            elif record.name.type == 'table':
                res[record.id] = record.survey_description
            elif record.name.type == 'date':
                res[record.id] = record.survey_description
            elif record.name.type == 'birth':
                description_1 = "0"
                if record.description_1:
                    description_1 = record.description_1
                survey_description = "0"
                if record.survey_description:
                    survey_description = record.survey_description
                res[record.id] = description_1 + " Year " + survey_description + " Month"
            elif record.name.type == 'numeric':
                res[record.id] = record.survey_description
            elif record.name.type == 'dropdown':
                res[record.id] = record.survey_description
            else:
                res[record.id] = False
        return res


    sequence = fields.Integer('Sequence')
    name = fields.Many2one('survey.question.bank', 'Question', required=True)
    random_id = fields.Char('Random')
    answer = fields.Selection([('option_a', 'Option 1'), ('option_b', 'Option 2'),
                                ('option_c', 'Option 3'), ('option_d', 'Option 4'),
                                ('option_e', 'Option 5'), ('option_f', 'Option 6'),
                                ('option_g', 'Option 7'), ('option_h', 'Option 8'),
                                ('option_i', 'Option 9'), ('option_j', 'Option 10'),
                                ('option_k', 'Option 11'), ('option_l', 'Option 12'),
                                ('option_m', 'Option 13'), ('option_n', 'Option 14'),
                                ('option_o', 'Option 15'), ('option_p', 'Option 16'),
                                ('option_q', 'Option 17'), ('option_r', 'Option 18'),
                                ('option_s', 'Option 19'), ('option_t', 'Option 20')], 'Answer')
    result = fields.Selection([('correct', 'Correct'), ('wrong', 'Wrong')], 'Result')
    type = fields.Selection([('multiple', 'Multiple Options'),
                              ('selection', 'Selection'),
                              ('multi_select', 'Multiple Selection'),
                              ('yes_no', 'Yes Or No'),
                              ('descriptive', 'Descriptive'),
                              ('title', 'Title'),
                              ('table', 'Table'),
                              ('date', 'Date'),
                              ('birth', 'Date of Birth'),
                              ('numeric', 'Numeric'),
                              ('dropdown', 'Drop-down')], 'Type of Question')
    choosen_survey_answer = fields.Char(compute='_get_survey_answer', string='Answers')
    yes_no = fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Yes / No')
    survey_description = fields.Text('Description')
    description = fields.Char('Description')
    description_1 = fields.Char('Description 1')
    description_2 = fields.Char('Description 2')
    common_note = fields.Char('Common Note')
    image = fields.Binary("Image", attachment=True)
    table_answers = fields.Html('Table Answers')
    medicine_dose = fields.Html('Medicine Dose')
    test_results_id = fields.Many2many('test.results', 'surveytest_result_question_rel', 'survey_result_id',
                                        'survey_question_id', 'Survey Result')



class TestResults(models.Model):
    _name = 'test.results'
    _description = 'Tests'
    _order = 'id desc'
    _rec_name = 'survey_test'

    def calculate_final_result(self, answers):
        ques_answered = 0
        ques_not_answered = 0
        correct_answers = 0
        actual_questions = 0
        res = {'ques_answered': 0,
               'ques_not_answered': 0,
               'correct_answers': 0,
               'final_result': '',
               'status': ''}
        q_a_pool = self.env['test.question.answer']
        for result in self.env['test.results'].search([('id','in', self.ids)]):
            for ques in result.ques_and_ans_ids:
                actual_questions += 1
            for ans in answers:
                if 'answer' in ans and ans['answer']:
                    for ques in result.ques_and_ans_ids:
                        if ques.name.id == ans['id']:
                            q_a_pool.write([ques.id], {'answer': ans['answer']})
                            ans_result = q_a_pool.check_answer([ques.id])
                            if ans_result:
                                correct_answers += 1
                            ques_answered += 1
                else:
                    ques_not_answered += 1
            pass_percent = (correct_answers * 100 / (ques_not_answered + ques_answered))
            if pass_percent >= result.name.pass_percentage:
                final_result = 'passed'
                status = 'completed'
                score = pass_percent
            else:
                final_result = 'failed'
                status = 'completed'
                score = pass_percent
            attempts = result.attempts + 1
            self.write([result.id], {'finalresult': final_result, 'attempts': attempts,
                                              'status': status, 'score': score, "test_or_survey": "test"})
            res = {'ques_answered': ques_answered,
                   'ques_not_answered': ques_not_answered,
                   'correct_answers': correct_answers,
                   'final_result': final_result,
                   'status': status,
                   'min_pass_percent': result.name.pass_percentage,
                   'pass_percentage': pass_percent}
        return res

    @api.multi
    def approve_test(self):
        self.write({'status': 'approved'})

    @api.multi
    def refuse_test(self):
        self.write({'status': 'refused'})

    # def record_survey_results(self, cr, uid, ids, survey_details, context=None):
    #     q_a_pool = self.pool.get('test.question.answer')
    #     result = ""
    #     q_a_ids = []
    #     for ans in survey_details['questions']:
    #         if 'answer' in ans and ans['answer']:
    #             if ans['answer'] == ans['correct_answer']:
    #                 result = "correct"
    #             else:
    #                 result = "wrong"
    #                 ques_ans_id = q_a_pool.create(cr,uid,{"name":ans['id'],"answer":ans['answer'],"result":result})
    #                 q_a_ids.append(ques_ans_id)
    #     record_id = self.create(cr,uid,{"name":survey_details['id'],
    #                                     "user_id":uid,"test_or_survey":"survey",
    #                                     "ques_and_ans_ids":[(6,0,q_a_ids)]})
    #     return True

    def record_survey_results(self, survey_details):
        q_a_pool = self.env['survey.test.question.answer']
        result = ""
        q_a_ids = []
        for ans in survey_details['questions']:
            question = self.env['survey.question.bank'].browse(ans['id'])
            sequence = question.sequence
            type = question.type
            description = ""
            if 'description' in ans and ans['description']:
                description = ans['description']
            if 'answer' in ans and ans['answer']:
                # _logger.info('Create a %s with ans.type %s', question.name, question.type)
                if question.type == 'multiple':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "answer": ans['answer'], "type": type,
                                                             "sequence": sequence, "description": description}))
                elif question.type == 'selection':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "answer": ans['answer'], "type": type,
                                                             "sequence": sequence, "description": description}))
                elif question.type == 'multi_select':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "answer": ans['answer'], "type": type,
                                                             "sequence": sequence, "description": description}))
                elif question.type == 'yes_no':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "yes_no": ans['answer'], "type": type,
                                                             "sequence": sequence, "description": description}))
                elif question.type == 'descriptive':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                             "survey_description": ans['answer']}))
                elif question.type == 'title':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                             "survey_description": ans['answer']}))
                elif question.type == 'date':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                             "survey_description": ans['answer']}))
                elif question.type == 'birth':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                             "survey_description": ans['answer']}))
                elif question.type == 'numeric':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                             "survey_description": ans['answer']}))
                elif question.type == 'dropdown':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                             "survey_description": ans['answer']}))
            else:
                if question.type == 'table' and 'table_answer' in ans and ans['table_answer']:
                    if ans["table_answer"][0]["viewRows"][0][question.table_head_2]:
                        table_answers = ans["table_answer"][0]["viewRows"]
                        table_final = ""
                        for table_answer in table_answers:
                            if question.table_head_2:
                                table_final = table_final + table_answer[question.table_head_2] + " | "
                            if question.table_head_3:
                                table_final = table_final + table_answer[question.table_head_3] + " | "
                            if question.table_head_4:
                                table_final = table_final + table_answer[question.table_head_4] + " | "
                            if question.table_head_5:
                                table_final = table_final + table_answer[question.table_head_5] + " | "
                            table_final = table_final + "\n"
                        q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                                 "table_answers": table_answers,
                                                                 "survey_description": table_final}).id)
                if question.is_input:
                    text = ""
                    if 'medicine' in ans and ans['medicine'] and 'dose' in ans and ans['dose']:
                        for medicine in ans['medicine']:
                            text = text + ans['medicine'].get(medicine) + " dose per day " + ans['dose'].get(
                                medicine) + "\n"
                        q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                                 "medicine_dose": ans, "survey_description": text}).id)
        return self.create({"survey_test": survey_details['id'], "user_id": uid, "test_or_survey": "survey",
                                     "status": "completed", "survey_ques_and_ans_ids": [(6, 0, q_a_ids)]})


    name = fields.Many2one('test', 'Test')
    random_id = fields.Char('Random')
    updated_at = fields.Char('Last Sync')
    user_id = fields.Many2one('res.users', 'User', defaults=lambda self: self.env.user)
    ques_and_ans_ids = fields.Many2many('test.question.answer', 'testresult_questions_rel', 'question_id',
                                         'result_id', 'Question Answers')
    survey_ques_and_ans_ids = fields.Many2many('survey.test.question.answer', 'surveytest_result_question_rel',
                                                'survey_question_id', 'survey_result_id', 'Survey Q/A')
    finalresult = fields.Selection([('passed', 'Passed'), ('failed', 'Failed')], 'Final Result')
#     type = fields.related('name', 'type', type='selection',
#                            selection=[('time_bound', 'Time Bound Test'),
#                                       ('one_by_one', 'One By One Test'),
#                                       ('time_bound_question', 'Time Bound Questions'),
#                                       ('adaptive', 'Adaptive Test')], string='Type')
    type = fields.Selection([('time_bound', 'Time Bound Test'),
                                          ('one_by_one', 'One By One Test'),
                                          ('time_bound_question', 'Time Bound Questions'),
                                          ('adaptive', 'Adaptive Test')], string='Type')
#     test_time = fields.related('name', 'test_time', type='float', string='Type')
    test_time = fields.Float(string='Type')
    test_or_survey = fields.Selection([('test', 'Test'), ('survey', 'Survey')], 'Test/Survey', default='test')
    status = fields.Selection([('new', 'New'),
                                ("incomplete", "Incomplete"),
                                ("completed", "Completed"),
                                ('waiting_approval', 'Waiting Approval'),
                                ('approved', 'Approved'),
                                ('refused', 'Refused')], 'Status')
    feedback = fields.Text("Feedback")
    survey_test = fields.Many2one('test', 'Survey')
    testprogress_id = fields.Many2one('test.progress', 'Test Progress')
    attempts = fields.Integer('Test Attempts')
    score = fields.Float('Score')
    count = fields.Integer('Answer Count', compute='_count_answers')
    image = fields.Binary("Image", attachment=True)


    @api.one
    @api.depends('survey_ques_and_ans_ids')
    def _count_answers(self):
        self.count = len(self.survey_ques_and_ans_ids)

    @api.multi
    def save_exit(self, survey_details, survey_id, status):
        _logger.info("Save and Exit")
        q_a_pool = self.env['survey.test.question.answer']
        q_a_ids = []
        result = ""
        for ans in survey_details['questions']:
            question = self.env['survey.question.bank'].browse(ans['id'])
            description = ""
            type = question.type
            sequence = question.sequence
            if 'description' in ans and ans['description']:
                description = ans['description']
            if 'answer' in ans and ans['answer']:
                for existing_ans_id in q_a_pool.search([('name', '=', ans['id'])]):
                    if existing_ans_id.test_results_id.id == survey_id:
                        existing_ans_id.unlink()
                if question.type == 'multiple':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "answer": ans['answer'], "type": type,
                                                    "sequence": sequence, "description": description}).id)
                elif question.type == 'selection':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "answer": ans['answer'], "type": type,
                                                    "sequence": sequence, "description": description}).id)
                elif question.type == 'multi_select':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "answer": ans['answer'], "type": type,
                                                    "sequence": sequence, "description": description}).id)
                elif question.type == 'yes_no':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "yes_no": ans['answer'], "type": type,
                                                    "sequence": sequence, "description": description}).id)
                elif question.type == 'descriptive':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                    "survey_description": ans['answer']}).id)
                elif question.type == 'title':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                    "survey_description": ans['answer']}).id)
                elif question.type == 'date':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                    "survey_description": ans['answer']}).id)
                elif question.type == 'birth':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                    "survey_description": ans['answer']}).id)
                elif question.type == 'numeric':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                    "survey_description": ans['answer']}).id)
                elif question.type == 'dropdown':
                    q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                    "survey_description": ans['answer']}).id)
            else:
                if question.type == 'table' and 'table_answer' in ans and ans['table_answer']:
                    if ans["table_answer"][0]["viewRows"][0][question.table_head_2]:
                        table_answers = ans["table_answer"][0]["viewRows"]
                        table_final = ""
                        for table_answer in table_answers:
                            if question.table_head_2:
                                table_final = table_final + table_answer[question.table_head_2] + " | "
                            if question.table_head_3:
                                table_final = table_final + table_answer[question.table_head_3] + " | "
                            if question.table_head_4:
                                table_final = table_final + table_answer[question.table_head_4] + " | "
                            if question.table_head_5:
                                table_final = table_final + table_answer[question.table_head_5] + " | "
                            table_final = table_final + "\n"
                        q_a_ids.append(q_a_pool.create({"name": ans['id'], "table_answers": table_answers, "type": type,
                                                        "sequence": sequence, "survey_description": table_final}).id)
                if question.is_input:
                    text = ""
                    if 'medicine' in ans and ans['medicine'] and 'dose' in ans and ans['dose']:
                        for medicine in ans['medicine']:
                            text = text + ans['medicine'].get(medicine) + " dose per day " + ans['dose'].get(
                                medicine) + "\n"
                        q_a_ids.append(q_a_pool.create({"name": ans['id'], "type": type, "sequence": sequence,
                                                        "medicine_dose": ans, "survey_description": text}).id)
        test_results_id = self.env['test.results'].browse(survey_id)
        survey_test_id = test_results_id.survey_test.id
        return test_results_id.write({"survey_test": survey_test_id, "test_or_survey": "survey",
                                      "status": status['status'], "survey_ques_and_ans_ids": [(4, q_a_ids)]})

    def get_survey_results(self):
        _logger.info("Get Survey Results By %s" % uid)
        surveys = []
        survey_ids = self.env['test.results'].search([('test_or_survey', '=', 'survey')])
        for survey in self.read(survey_ids, []):
            survey['answers'] = []
            survey['res_users_details'] = []
            survey['res_users_details'].append({"user_id": uid})
            for ans in survey['survey_ques_and_ans_ids']:
                ans_id = self.env['survey.test.question.answer']
                survey['answers'].append(ans_id.read(ans, []))
            surveys.append(survey)
        return {"records": surveys}

    @api.multi
    def get_answers(self, survey_ans_id, survey_q_id):
        res = {}
        records = []
        answer_array = []
        survey_answer_ids = self.env['survey.test.question.answer'].search([('test_results_id', '=', survey_ans_id)],
                                                                           order='sequence asc')
        survey_question_ids = self.env['survey.question.bank'].search([('id', '=', survey_q_id)])
        for answer in survey_answer_ids:
            answer_array.append(answer.id)
        for answer in survey_answer_ids:
            if answer_array:
                ans = answer_array.pop()
                ans_id = self.env['survey.test.question.answer'].search([('id', '=', ans)])
            if survey_q_id == ans_id.name.id and answer_array:
                previous_ans_id = answer_array.pop()
                previous_ans = self.env['survey.test.question.answer'].search([('id', '=', previous_ans_id)])
                records = previous_ans.read(['name', 'answer', 'result', 'type', 'choosen_survey_answer', 'yes_no',
                                             'image', 'survey_description', 'description', 'description_1',
                                             'description_2', 'table_answers', 'test_results_id'])
        return {'records': records}

    @api.multi
    def get_previous_answer(self, survey_ans_id, survey_q_id):
        _logger.info("Get Previous Answer")
        res = {}
        records = []
        answer_array = []
        survey_answer_ids = self.env['survey.test.question.answer'].search([('test_results_id', '=', survey_ans_id)],
                                                                           order='sequence asc')
        survey_question_ids = self.env['survey.question.bank'].search([('id', '=', survey_q_id)])
        for answer in survey_answer_ids:
            answer_array.append(answer.id)
        for answer in survey_answer_ids:
            if answer_array:
                ans = answer_array[-1]
                ans_id = self.env['survey.test.question.answer'].search([('id', '=', ans)])
                pop_ans = answer_array.pop()
            if survey_q_id == ans_id.name.id and answer_array:
                previous_ans_id = answer_array.pop()
                previous_ans = self.env['survey.test.question.answer'].search([('id', '=', previous_ans_id)])
                records = previous_ans.read(['name', 'answer', 'result', 'type', 'choosen_survey_answer', 'yes_no',
                                             'image', 'survey_description', 'description', 'description_1',
                                             'description_2', 'table_answers', 'test_results_id'])
        return {'records': records}

    @api.multi
    def get_next_answer(self, survey_ans_id, survey_q_id):
        _logger.info("Get Next Answer")
        res = {}
        records = []
        answer_array = []
        survey_answer_ids = self.env['survey.test.question.answer'].search([('test_results_id', '=', survey_ans_id)],
                                                                           order='sequence asc')
        survey_question_ids = self.env['survey.question.bank'].search([('id', '=', survey_q_id)])
        for answer in survey_answer_ids:
            answer_array.append(answer.id)
        for answer in survey_answer_ids:
            if answer_array:
                next_ans = answer_array.pop()
                next_ans_id = self.env['survey.test.question.answer'].search([('id', '=', next_ans)])
            if answer_array:
                current_ans = answer_array[-1]
                current_ans_id = self.env['survey.test.question.answer'].search([('id', '=', current_ans)])
                if survey_q_id == current_ans_id.name.id and answer_array:
                    records = next_ans_id.read(['name', 'answer', 'result', 'type', 'choosen_survey_answer', 'yes_no',
                                                'image', 'survey_description', 'description', 'description_1',
                                                'description_2', 'table_answers', 'test_results_id'])
        return {'records': records}


class SurveyQuestionBank(models.Model):
    _name = 'survey.question.bank'
    _description = 'Survey Question Bank'
    _order = 'sequence'

    def _get_table_text(self):
        result = {}
        for table in self.env['survey.question.bank'].search([('id','in',self.ids)]):
            data = {}
            column = []
            row = []
            if table.table_head_1:
                column.append(table.table_head_1)
            if table.table_head_2:
                column.append(table.table_head_2)
            if table.table_head_3:
                column.append(table.table_head_3)
            if table.table_head_4:
                column.append(table.table_head_4)
            if table.table_head_5:
                column.append(table.table_head_5)
            if table.table_row_1:
                row.append(table.table_row_1)
            if table.table_row_2:
                row.append(table.table_row_2)
            if table.table_row_3:
                row.append(table.table_row_3)
            if table.table_row_4:
                row.append(table.table_row_4)
            if table.table_row_5:
                row.append(table.table_row_5)
            if table.table_row_6:
                row.append(table.table_row_6)
            if table.table_row_7:
                row.append(table.table_row_7)
            if table.table_row_8:
                row.append(table.table_row_8)
            if table.table_row_9:
                row.append(table.table_row_9)
            if table.table_row_10:
                row.append(table.table_row_10)
            if table.table_row_11:
                row.append(table.table_row_11)
            if table.table_row_12:
                row.append(table.table_row_12)
            if table.table_row_13:
                row.append(table.table_row_13)
            if table.table_row_14:
                row.append(table.table_row_14)
            if table.table_row_15:
                row.append(table.table_row_15)
            if table.table_row_16:
                row.append(table.table_row_16)
            if table.table_row_17:
                row.append(table.table_row_17)
            if table.table_row_18:
                row.append(table.table_row_18)
            if table.table_row_19:
                row.append(table.table_row_19)
            if table.table_row_20:
                row.append(table.table_row_20)
            if table.table_row_21:
                row.append(table.table_row_21)
            if table.table_row_22:
                row.append(table.table_row_22)
            data = {"column": column, "row": row}
            result[table.id] = json.dumps(data)
        return result


    name = fields.Char('Question', required=True)
    sequence = fields.Integer('Sequence', help="Gives the sequence order in Integer.", copy=False)
    option_a = fields.Many2one('survey.answer.bank', 'Option 1')
    option_b = fields.Many2one('survey.answer.bank', 'Option 2')
    option_c = fields.Many2one('survey.answer.bank', 'Option 3')
    option_d = fields.Many2one('survey.answer.bank', 'Option 4')
    option_e = fields.Many2one('survey.answer.bank', 'Option 5')
    option_f = fields.Many2one('survey.answer.bank', 'Option 6')
    option_g = fields.Many2one('survey.answer.bank', 'Option 7')
    option_h = fields.Many2one('survey.answer.bank', 'Option 8')
    option_i = fields.Many2one('survey.answer.bank', 'Option 9')
    option_j = fields.Many2one('survey.answer.bank', 'Option 10')
    option_k = fields.Many2one('survey.answer.bank', 'Option 11')
    option_l = fields.Many2one('survey.answer.bank', 'Option 12')
    option_m = fields.Many2one('survey.answer.bank', 'Option 13')
    option_n = fields.Many2one('survey.answer.bank', 'Option 14')
    option_o = fields.Many2one('survey.answer.bank', 'Option 15')
    option_p = fields.Many2one('survey.answer.bank', 'Option 16')
    option_q = fields.Many2one('survey.answer.bank', 'Option 17')
    option_r = fields.Many2one('survey.answer.bank', 'Option 18')
    option_s = fields.Many2one('survey.answer.bank', 'Option 19')
    option_t = fields.Many2one('survey.answer.bank', 'Option 20')
    multi_select = fields.Many2many('survey.answer.bank', 'multi_select_rel', 'survey_id', 'question_id',
                                     'Multiple Selection')
    title = fields.Char('Title')
    is_calculate = fields.Boolean('Calculate', help="If you want to 'Calculate' enable this.")
    is_display = fields.Boolean('Display', help="If you want to 'Display' enable this.")
    is_bar = fields.Boolean('Bar', help="If you need 'Bar' enable this.")
    is_input = fields.Boolean('Input', help="If you need 'Input' enable this.")
    is_image = fields.Boolean('Image', help="If you need 'Image' enable this.")
    is_date = fields.Boolean('Multiple Date', help="If you need 'Multiple Date' enable this.")
    is_note = fields.Boolean('Note', help="If you need 'Note' enable this.")
    note = fields.Char('Note')
    is_placeholder = fields.Boolean('Placeholder', help="If you need 'Placeholder' enable this.")
    placeholder = fields.Char('Placeholder')
    is_dropdown = fields.Boolean('Dropdown', help="If you need 'Dropdown' enable this.")
    dropdown_option = fields.Selection([('option_a', 'Option 1'), ('option_b', 'Option 2'),
                                         ('option_c', 'Option 3'), ('option_d', 'Option 4'),
                                         ('option_e', 'Option 5'), ('option_f', 'Option 6'),
                                         ('option_g', 'Option 7'), ('option_h', 'Option 8'),
                                         ('option_i', 'Option 9'), ('option_j', 'Option 10'),
                                         ('option_k', 'Option 11'), ('option_l', 'Option 12'),
                                         ('option_m', 'Option 13'), ('option_n', 'Option 14'),
                                         ('option_o', 'Option 15'), ('option_p', 'Option 16'),
                                         ('option_q', 'Option 17'), ('option_r', 'Option 18'),
                                         ('option_s', 'Option 19'), ('option_t', 'Option 20')],
                                        string='Option for Dropdown')
    is_hour = fields.Boolean('Hour', help="If you need 'Hour' enable this.")
    hour_option = fields.Selection([('option_a', 'Option 1'), ('option_b', 'Option 2'),
                                     ('option_c', 'Option 3'), ('option_d', 'Option 4'),
                                     ('option_e', 'Option 5'), ('option_f', 'Option 6'),
                                     ('option_g', 'Option 7'), ('option_h', 'Option 8'),
                                     ('option_i', 'Option 9'), ('option_j', 'Option 10'),
                                     ('option_k', 'Option 11'), ('option_l', 'Option 12'),
                                     ('option_m', 'Option 13'), ('option_n', 'Option 14'),
                                     ('option_o', 'Option 15'), ('option_p', 'Option 16'),
                                     ('option_q', 'Option 17'), ('option_r', 'Option 18'),
                                     ('option_s', 'Option 19'), ('option_t', 'Option 20')],
                                    string='Option for Hour')
    is_description = fields.Boolean('Description', help="If you need 'Description' enable this.")
    description_option_1 = fields.Selection([('option_a', 'Option 1'), ('option_b', 'Option 2'),
                                              ('option_c', 'Option 3'), ('option_d', 'Option 4'),
                                              ('option_e', 'Option 5'), ('option_f', 'Option 6'),
                                              ('option_g', 'Option 7'), ('option_h', 'Option 8'),
                                              ('option_i', 'Option 9'), ('option_j', 'Option 10'),
                                              ('option_k', 'Option 11'), ('option_l', 'Option 12'),
                                              ('option_m', 'Option 13'), ('option_n', 'Option 14'),
                                              ('option_o', 'Option 15'), ('option_p', 'Option 16'),
                                              ('option_q', 'Option 17'), ('option_r', 'Option 18'),
                                              ('option_s', 'Option 19'), ('option_t', 'Option 20')],
                                             string='Option 1 for Description')
    description_option_2 = fields.Selection([('option_a', 'Option 1'), ('option_b', 'Option 2'),
                                              ('option_c', 'Option 3'), ('option_d', 'Option 4'),
                                              ('option_e', 'Option 5'), ('option_f', 'Option 6'),
                                              ('option_g', 'Option 7'), ('option_h', 'Option 8'),
                                              ('option_i', 'Option 9'), ('option_j', 'Option 10'),
                                              ('option_k', 'Option 11'), ('option_l', 'Option 12'),
                                              ('option_m', 'Option 13'), ('option_n', 'Option 14'),
                                              ('option_o', 'Option 15'), ('option_p', 'Option 16'),
                                              ('option_q', 'Option 17'), ('option_r', 'Option 18'),
                                              ('option_s', 'Option 19'), ('option_t', 'Option 20')],
                                             string='Option 2 for Description')
    table_row = fields.Integer('Table Row')
    table_column = fields.Integer('Table Column')
    get_table_text = fields.Html(compute='_get_table_text', string='Table Text JSON')
    table_row_1 = fields.Char('Row 1')
    table_row_2 = fields.Char('Row 2')
    table_row_3 = fields.Char('Row 3')
    table_row_4 = fields.Char('Row 4')
    table_row_5 = fields.Char('Row 5')
    table_row_6 = fields.Char('Row 6')
    table_row_7 = fields.Char('Row 7')
    table_row_8 = fields.Char('Row 8')
    table_row_9 = fields.Char('Row 9')
    table_row_10 = fields.Char('Row 10')
    table_row_11 = fields.Char('Row 11')
    table_row_12 = fields.Char('Row 12')
    table_row_13 = fields.Char('Row 13')
    table_row_14 = fields.Char('Row 14')
    table_row_15 = fields.Char('Row 15')
    table_row_16 = fields.Char('Row 16')
    table_row_17 = fields.Char('Row 17')
    table_row_18 = fields.Char('Row 18')
    table_row_19 = fields.Char('Row 19')
    table_row_20 = fields.Char('Row 20')
    table_row_21 = fields.Char('Row 21')
    table_row_22 = fields.Char('Row 22')
    table_head_1 = fields.Char('Heading 1')
    table_head_2 = fields.Char('Heading 2')
    table_head_3 = fields.Char('Heading 3')
    table_head_4 = fields.Char('Heading 4')
    table_head_5 = fields.Char('Heading 5')
    table_head_6 = fields.Char('Heading 6')
    answer_id = fields.One2many('survey.answer.bank', 'question_id', 'Survey Answers')
    type = fields.Selection([('multiple', 'Multiple Options'),
                              ('selection', 'Selection'),
                              ('multi_select', 'Multiple Selection'),
                              ('yes_no', 'Yes Or No'),
                              ('descriptive', 'Descriptive'),
                              ('title', 'Title'),
                              ('table', 'Table'),
                              ('date', 'Date'),
                              ('birth', 'Date of Birth'),
                              ('numeric', 'Numeric'),
                              ('dropdown', 'Drop-down')], 'Type of Question', required=True)
    option_desc = fields.Many2one('survey.answer.bank', 'Loop for next question')
    yes_heading = fields.Char('Yes Heading')
    no_heading = fields.Char('No Heading')
    description_type = fields.Selection([('always', 'Always'), ('yes', 'Yes'), ('no', 'No'), ('never', 'Never')],
                                         'Descriptive Type', default='never')
    survey = fields.Many2many('test', 'survey_questions_rel', 'survey_id', 'questions_id', 'Survey',
                               readonly=True)


#     _defaults = {
#         'user_id': lambda obj, cr, uid, context: uid,
#     }

    _sql_constraints = [
        ('unique_sequence', 'unique(sequence)', "Sequence no. already exists. Sequence no. must be unique!"),
    ]

    @api.one
    @api.constrains('table_row', 'table_column')
    def _check_table_row_column(self):
        if self.table_row > 22:
            raise Warning(_("Maximum Table Row limit is 22."))
        if self.table_column > 6:
            raise Warning(_("Maximum Table Column limit is 6."))

    @api.multi
    def _sequence_cron(self):
        _logger.info("Cron job for Changing Sequence to Next")
        for retail in self.search([], order='sequence DESC'):
            retail.sequence = retail.sequence + 1
        return True


class SurveyAnswerBank(models.Model):
    _name = 'survey.answer.bank'
    _description = 'Survey Answer Bank'


    name = fields.Char('Answer', required=True)
    end_survey = fields.Boolean('End Survey', help="If True, Survey will end by this question.")
    question_id = fields.Many2one('survey.question.bank', 'Survey Questions')


    @api.onchange('end_survey')
    def onchange_end_survey(self):
        if self.end_survey:
            self.question_id = ''
        return {}


class ProductQuestionBank(models.Model):
    _name = 'product.question.bank'
    _description = 'Product Question Bank'
    _order = 'sequence'


    name = fields.Char('Question', required=True)
    sequence = fields.Integer('Sequence', required=True, help="Gives the sequence order in Integer.")
    subject = fields.Char('Subject')
    option_a = fields.Char('Option 1', required=True)
    option_b = fields.Char('Option 2', required=True)
    option_c = fields.Char('Option 3')
    option_d = fields.Char('Option 4')
    option_e = fields.Char('Option 5')
    option_f = fields.Char('Option 6')
    option_g = fields.Char('Option 7')
    option_h = fields.Char('Option 8')
    option_i = fields.Char('Option 9')
    option_j = fields.Char('Option 10')
    option_k = fields.Char('Option 11')
    option_l = fields.Char('Option 12')
    option_m = fields.Char('Option 13')
    option_n = fields.Char('Option 14')
    option_o = fields.Char('Option 15')
    option_p = fields.Char('Option 16')
    option_q = fields.Char('Option 17')
    option_r = fields.Char('Option 18')
    option_s = fields.Char('Option 19')
    option_t = fields.Char('Option 20')
    correct_answer = fields.Selection([('option_a', 'Option 1'), ('option_b', 'Option 2'),
                                        ('option_c', 'Option 3'), ('option_d', 'Option 4'),
                                        ('option_e', 'Option 5'), ('option_f', 'Option 6'),
                                        ('option_g', 'Option 7'), ('option_h', 'Option 8'),
                                        ('option_i', 'Option 9'), ('option_j', 'Option 10'),
                                        ('option_k', 'Option 11'), ('option_l', 'Option 12'),
                                        ('option_m', 'Option 13'), ('option_n', 'Option 14'),
                                        ('option_o', 'Option 15'), ('option_p', 'Option 16'),
                                        ('option_q', 'Option 17'), ('option_r', 'Option 18'),
                                        ('option_s', 'Option 19'), ('option_t', 'Option 20')], 'Correct Answer',
                                       required=True)
    difficulty = fields.Selection([('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')],
                                   'Question Difficulty')
    author = fields.Many2one('res.users', 'Author')
    # 'related_to': fields.many2one('product.information', 'Product'),
    ques_set_id = fields.Many2one('question.set', 'Question Set')
    time = fields.Float('Time')


#     _defaults = {
#         'user_id': lambda obj, cr, uid, context: uid,
#         'sequence': lambda *a: 1,
#     }


class QuestionSet(models.Model):
    _name = 'question.set'
    _description = 'Question Set'


    name = fields.Char('Question Set Name', required=True)



class UserSurveys(models.Model):
    _name = 'user.surveys'
    _description = 'User Surveys'
    _rec_name = 'user_id'


    user_id = fields.Many2one('res.users', 'User Id', required=True)
    checked = fields.Boolean("Checked", default=True)


#     _sql_constraints = [
#         ('unique_user_id', 'unique(user_id)', "A User already exists with this name. User's name must be unique!"),
#     ]


class ResUsers(models.Model):
    _inherit = 'res.users'


    checked = fields.Boolean("Checked", default=True)



class SelfCertification(models.Model):
    _name = 'self.certification'
    _description = 'Self Certification'


    name = fields.Char('Name', required=True)
    user_id = fields.Many2one('res.users', 'User')
    document_ids = fields.Many2many('ir.attachment', 'certification_attachment_rel', 'attachment_id',
                                     'certification_id', 'Document')
    agree_date = fields.Datetime("Agreed On")

    @api.model
    def create(self,vals):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        name = vals['name']
        user_ids = self.env['res.users'].search([('id', 'in', [vals['user_id']])])
        result = super(SelfCertification, self).create(vals)
        certify_id = result
        for user in self.env['res.users'].browse(user_ids.ids):
            tokens = []
            if user.user_tokens:
                for token in self.env['user.token'].browse(user.user_tokens.ids):
                    tokens.append(token.name)
        data_json = {"channeltype": "newCertification",
                     "name": name,
                     "message": "New Certification %s has been created" % name,
                     "certify_id": certify_id.ids}
        data = {'notification': {'title': name,
                                 'body': "New Certification %s has been created" % name,
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return result

    def unlink(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        for page_id in self.env['self.certification'].search([('id','in', self.ids)]):
            certify_id = page_id.id
            name = page_id.name
            user_ids = self.env['res.users'].search([('id', 'in', [page_id.user_id.id])])
            for user in self.env['res.users'].browse(user_ids):
                tokens = []
                if user.user_tokens:
                    for token in self.env['user.token'].browse(user.user_tokens.id):
                        tokens.append(token.name)
            data_json = {"channeltype": "newCertification",
                         "name": name,
                         "message": "The Certification %s has been removed" % name,
                         'certify_id': certify_id.ids}
            data = {'notification': {'title': name,
                                     'body': "The Certification %s has been removed" % name,
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))
        return super(SelfCertification, self).unlink(self.ids)
